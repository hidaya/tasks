package com.microService;

import java.util.stream.Stream;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.microService.entities.Company;
import com.microService.repositories.CampanyRepo;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
@Bean
CommandLineRunner start(CampanyRepo campanyRepo) {
	return args->{
		Stream.of("A","B","C").forEach(cn->{campanyRepo.save(new Company(null,cn,100+Math.random()*900));});
campanyRepo.findAll().forEach(System.out::println);
	};}
}
