package com.microService.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.microService.entities.Company;
@RepositoryRestResource
public interface CampanyRepo extends JpaRepository<Company, Long> {

}
