package com.microService.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Table(name = "Campany")
public class Company {
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY )
private Long id;
	@Column(name = "name")
private String name;
	@Column(name = "price")
private double price;
}
