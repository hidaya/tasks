package com.bank.services;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.bank.dtos.CustomerDto;
import com.bank.entities.Customer;
import com.bank.exception.CustomerNotFoundException;
import com.bank.mappers.ModelMapperConverter;
import com.bank.repositories.CustomerRepository;

@SpringBootTest
@TestPropertySource("classpath:application.properties")

public class customerServiceTest {
@Autowired
BankAccountService bankService;
 @Autowired
 CustomerRepository customerRepository;

@Test
@Sql(scripts="/Customer_data.sql",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD) 
@Sql(scripts="/delete_Customer_data.sql",executionPhase = ExecutionPhase.AFTER_TEST_METHOD) 
void testgetCustomerById() throws CustomerNotFoundException {
	assertEquals("hideya", bankService.getCustomer(1L).getName());
}

@Test
@Sql(scripts="/Customer_data.sql",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD) 
void deleteCustomer(){
	 int size=customerRepository.findAll().size();
	 assertEquals(1, size);
	 bankService.deleteCustomer(1L);
	 assertEquals(0, customerRepository.findAll().size());
 }

@Test

void addCustomer() throws CustomerNotFoundException {
	CustomerDto customer=new CustomerDto();
	customer.setEmail("test@gmail.com");
	customer.setName("test");
	customer.setId(2L);
	bankService.saveCustomer(customer);
	 assertEquals("test",bankService.getCustomer(2L).getName());
	 assertEquals(1,customerRepository.findAll().size());
}
@Test
@Sql(scripts="/Customer_data.sql",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD) 
@Sql(scripts="/delete_Customer_data.sql",executionPhase = ExecutionPhase.AFTER_TEST_METHOD) 
void updateCustomer() throws CustomerNotFoundException {
	CustomerDto customer=new CustomerDto();
	customer.setEmail("x@gmail.com");
	customer.setName("x");
	System.out.println("dd"+customer);
	System.out.println("all"+customerRepository.findAll());
	//CustomerDto cust=bankService.updateCustomer(1L, customer);
	//System.out.println("ff"+cust);
	assertEquals("x", bankService.updateCustomer(1L, customer).getName());
	

}
}
