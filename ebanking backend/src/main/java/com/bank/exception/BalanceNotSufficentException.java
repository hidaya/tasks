package com.bank.exception;

public class BalanceNotSufficentException extends Exception {

	public BalanceNotSufficentException(String message) {
		super(message);
	}
}

/**
 * 
 */
