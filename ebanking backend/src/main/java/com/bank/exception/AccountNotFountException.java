package com.bank.exception;

public class AccountNotFountException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AccountNotFountException(String message) {
	        super(message);
	    }

}
