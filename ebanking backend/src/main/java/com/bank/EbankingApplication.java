package com.bank;

import java.sql.Date;
import java.util.UUID;
import java.util.stream.Stream;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.bank.dtos.CustomerDto;
import com.bank.entities.AccountOperation;
import com.bank.entities.CurrentAccount;
import com.bank.entities.Customer;
import com.bank.entities.SavingAccount;
import com.bank.enums.AccountStatus;
import com.bank.enums.operationType;
import com.bank.exception.CustomerNotFoundException;
import com.bank.repositories.AccountOperationRepository;
import com.bank.repositories.BankAccountRepository;
import com.bank.repositories.CustomerRepository;
import com.bank.services.BankAccountService;


@SpringBootApplication
public class EbankingApplication {

	public static void main(String[] args) {
		SpringApplication.run(EbankingApplication.class, args);
			
	}
	
}
	 
