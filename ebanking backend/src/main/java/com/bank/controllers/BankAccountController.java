package com.bank.controllers;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bank.dtos.AccountOperationDto;
import com.bank.dtos.BankAccountDto;
import com.bank.dtos.CreditDto;
import com.bank.dtos.CurrentAccountDto;
import com.bank.dtos.CustomerDto;
import com.bank.dtos.DebitDto;
import com.bank.dtos.SavingBankAccountDto;
import com.bank.dtos.TransferDto;
import com.bank.exception.AccountNotFountException;
import com.bank.exception.BalanceNotSufficentException;
import com.bank.exception.CustomerNotFoundException;
import com.bank.services.BankAccountService;

import lombok.AllArgsConstructor;
import lombok.var;

@RestController
@AllArgsConstructor
@RequestMapping("/accounts")
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
public class BankAccountController {

	private BankAccountService bankAccountService;

	@GetMapping("/{id}")
	public BankAccountDto getBankAccount(@PathVariable String id) throws AccountNotFountException {

		return bankAccountService.getBankAccount(id);
	}

	@GetMapping
	public List<BankAccountDto> getAllBankAccounts() {
		return bankAccountService.bankAountList();
	}
	@GetMapping("/customer/{id}")
	public List<BankAccountDto> getAllBankAccountsByUserId(@PathVariable Long id) {
		return bankAccountService.getAccountsByUserId(id);
	}
	@PostMapping("/savingCurrent")
	public CurrentAccountDto saveCurrentAccount( @RequestBody CurrentAccountDto currentAccount)
			throws CustomerNotFoundException {
		return bankAccountService.saveCurrentBankAccount( currentAccount.getCustomer().getId(),currentAccount.getBalance(), currentAccount.getOverDraft());
	}
	@PostMapping("/savingSaving")
	public SavingBankAccountDto saveCurrentAccount( @RequestBody SavingBankAccountDto savingAccountDto)
			throws CustomerNotFoundException {
		return bankAccountService.saveSavingBankAccount( savingAccountDto.getCustomer().getId(), savingAccountDto.getBalance(), savingAccountDto.getInterestedRate());
	}

	@GetMapping("/history/{id}")
	public List<AccountOperationDto> getAllHistory(@PathVariable String id) {
		return bankAccountService.accountHistory(id);
	}

	@GetMapping("/pagehistory/{accountId}")
	public Page<AccountOperationDto> getAllHistoryPaginate(@PathVariable String accountId,
			@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "2") int size) {
		return bankAccountService.getAllUsersPagination(accountId, page, size);
	}

	@PostMapping("/debit")
	public DebitDto debitAccount( @RequestBody DebitDto debitDto)throws AccountNotFountException, BalanceNotSufficentException {
		System.out.println(debitDto);
		bankAccountService.debit(debitDto.getAccountId(), debitDto.getAmount(), debitDto.getDescription());
		return debitDto;
	}

	@PostMapping("/credit")
	public CreditDto creditAccount( @RequestBody CreditDto creditDto)
			throws AccountNotFountException {
		bankAccountService.credit(creditDto.getAccountId(), creditDto.getAmount(), creditDto.getDescription());
		return creditDto;
	}
	

	@GetMapping("/pageAccounts")
	public Page<BankAccountDto> getAllAccountsPaginate(
			@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "2") int size) {
		return bankAccountService.getAllAccountsPagination( page, size);
	}
	
	@PostMapping("/transfer")
	public TransferDto transferAccount( @RequestBody TransferDto transferDto)throws AccountNotFountException, BalanceNotSufficentException {
		System.out.println(transferDto);
		bankAccountService.transfer(transferDto.getAccountId(), transferDto.getAccountDestination(), transferDto.getAmount());
		return transferDto;
	}
	@GetMapping("/search")

	public  Page<BankAccountDto> filterByAccount(@RequestParam(required = false) String key,
			@RequestParam(required = false) int page, @RequestParam(required = false) int size) {

		return bankAccountService.rechercheAccount(key,page, size);
	}

	
}