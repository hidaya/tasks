package com.bank.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bank.dtos.AuthResponse;
import com.bank.dtos.RegisterResponse;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/bank/auth")
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RequiredArgsConstructor
public class AuthenticationController {

	private final com.bank.services.AuthenticationService service;

	@PostMapping("/register")
	public ResponseEntity<RegisterResponse> register(@RequestBody com.bank.dtos.RegisterDto request) {

		String msg = service.register(request);
		if (msg ==null)
			return new ResponseEntity<>(new RegisterResponse("Ce compte existe déja"),
					HttpStatus.BAD_REQUEST);
		return new ResponseEntity<>(new RegisterResponse(msg), HttpStatus.CREATED);

	}

	@PostMapping("/login")
	public ResponseEntity<AuthResponse> authenticate(@RequestBody com.bank.dtos.LoginDto request) {
		return ResponseEntity.ok(service.authenticate(request));
	}

}
