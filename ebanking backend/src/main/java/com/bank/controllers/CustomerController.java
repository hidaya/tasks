package com.bank.controllers;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bank.dtos.CustomerDto;
import com.bank.exception.CustomerNotFoundException;
import com.bank.services.BankAccountService;

import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
 @RequestMapping("/customers")
@CrossOrigin(origins = "http://localhost:4200")
public class CustomerController {
	private BankAccountService bankAccountService;

	@GetMapping
	public List<CustomerDto> customers() {
		return bankAccountService.listCustomers();
	}
	@GetMapping("/page")
	public Page<CustomerDto> customersPaginate(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "2") int size) {
		return bankAccountService.getAllCustomersPagination(page, size);
	}
	@GetMapping("/{id}")
	public CustomerDto getCustomer(@PathVariable Long id) throws CustomerNotFoundException {

		return bankAccountService.getCustomer(id);
	}

	@PostMapping
	public CustomerDto saveCustomer(@RequestBody CustomerDto customer) {

		return bankAccountService.saveCustomer(customer);
	}

	@PutMapping("/{id}")
	public CustomerDto updateCustomer(@RequestBody CustomerDto customer, @PathVariable Long id) {

		return bankAccountService.updateCustomer(id, customer);
	}

	@DeleteMapping("/{id}")
	public void deleteCustomer(@PathVariable Long id) {
		bankAccountService.deleteCustomer(id);
	}
	@GetMapping("/search")

	public Page<CustomerDto> filterByName(@RequestParam(required = false) String key,
			@RequestParam(required = false) int page, @RequestParam(required = false) int size) {

		return bankAccountService.rechercheUser(key,page, size);
	}



}
