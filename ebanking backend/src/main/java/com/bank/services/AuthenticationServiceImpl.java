package com.bank.services;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.bank.config.JwtService;
import com.bank.dtos.AuthResponse;
import com.bank.dtos.LoginDto;
import com.bank.dtos.RegisterDto;
import com.bank.entities.Customer;
import com.bank.enums.Roles;
import com.bank.repositories.CustomerRepository;


import lombok.RequiredArgsConstructor;
import lombok.var;

@Service
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {

	private final CustomerRepository repository;
	private final PasswordEncoder passwordEncoder;
	private final JwtService jwtService;
	private final AuthenticationManager authenticationManager;
	
	@Override
	public String register(RegisterDto request) {


		if (repository.existsByEmail(request.getEmail()))
			return null;
		var user = Customer.builder().firstName(request.getFirstName()).lastName(request.getLastName())
				.email(request.getEmail()).password(passwordEncoder.encode(request.getPassword())).role(Roles.USER).status(true)
				.build();
		repository.save(user);
		
		return "Le compte est créé avec succès";
	}
	
	@Override
	public AuthResponse authenticate(LoginDto request) {
		Authentication authentication = authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword()));
		var user = repository.findByEmail(request.getEmail()).orElseThrow();
		var jwtToken = jwtService.generateToken(user);
		SecurityContextHolder.getContext().setAuthentication(authentication);
	
	
		return AuthResponse.builder().token(jwtToken).build();
	}

}
