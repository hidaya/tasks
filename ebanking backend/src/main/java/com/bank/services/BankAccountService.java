package com.bank.services;

import java.util.List;

import org.springframework.data.domain.Page;


import com.bank.dtos.AccountOperationDto;
import com.bank.dtos.BankAccountDto;
import com.bank.dtos.CurrentAccountDto;
import com.bank.dtos.CustomerDto;
import com.bank.dtos.SavingBankAccountDto;
import com.bank.exception.AccountNotFountException;
import com.bank.exception.BalanceNotSufficentException;
import com.bank.exception.CustomerNotFoundException;



public interface BankAccountService {

	CustomerDto saveCustomer(CustomerDto customer);

	CurrentAccountDto saveCurrentBankAccount(Long customerId, double intitalBalance, double overDraft)
			throws CustomerNotFoundException;

	SavingBankAccountDto saveSavingBankAccount(Long customerId, double intitalBalance, double interestRate)
			throws CustomerNotFoundException;

	List<CustomerDto> listCustomers();

	BankAccountDto getBankAccount(String accountID) throws AccountNotFountException;

	void credit(String accountId, double amount, String description) throws AccountNotFountException;

	void transfer(String intialIdAccount, String accountIdDestination, double amount)
			throws AccountNotFountException, BalanceNotSufficentException;

	List<BankAccountDto> bankAountList();

	CustomerDto getCustomer(Long customerId) throws CustomerNotFoundException;

	void deleteCustomer(Long customerId);

	CustomerDto updateCustomer(Long customerId, CustomerDto newCustomerDto);

	List<AccountOperationDto> accountHistory(String id);

	Page<AccountOperationDto> getAllUsersPagination(String accountId, int page, int size);

	Page<BankAccountDto> getAllAccountsPagination(int page, int size);

	Page<CustomerDto> getAllCustomersPagination(int page, int size);

	void debit(String accountId, double amount, String description)
			throws AccountNotFountException, BalanceNotSufficentException;

	Page<CustomerDto> rechercheUser(String key, int page, int size);
	 Page<BankAccountDto> rechercheAccount(String key, int page, int size);
	List<BankAccountDto>getAccountsByUserId(Long id);


}
