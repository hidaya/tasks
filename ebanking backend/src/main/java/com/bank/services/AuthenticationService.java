package com.bank.services;

import com.bank.dtos.AuthResponse;
import com.bank.dtos.LoginDto;
import com.bank.dtos.RegisterDto;

public interface AuthenticationService {
	
	public String register(RegisterDto request);
	
	public AuthResponse authenticate(LoginDto request);

}
