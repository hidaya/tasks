package com.bank.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bank.dtos.AccountOperationDto;
import com.bank.dtos.BankAccountDto;
import com.bank.dtos.CurrentAccountDto;
import com.bank.dtos.CustomerDto;
import com.bank.dtos.SavingBankAccountDto;
import com.bank.entities.AccountOperation;
import com.bank.entities.BankAccount;
import com.bank.entities.CurrentAccount;
import com.bank.entities.Customer;
import com.bank.entities.SavingAccount;
import com.bank.enums.operationType;
import com.bank.exception.AccountNotFountException;
import com.bank.exception.BalanceNotSufficentException;
import com.bank.exception.CustomerNotFoundException;
import com.bank.mappers.ModelMapperConverter;
import com.bank.repositories.AccountOperationRepository;
import com.bank.repositories.BankAccountRepository;
import com.bank.repositories.CustomerRepository;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@Slf4j
@AllArgsConstructor
public class BankAccountServiceImpl implements BankAccountService {

	private AccountOperationRepository accountOperationRepository;
	private BankAccountRepository bankAccountRepository;
	private CustomerRepository customerRepository;

	@Override
	public CustomerDto saveCustomer(CustomerDto customerDto) {

		Customer customer = ModelMapperConverter.map(customerDto, Customer.class);
		customer.setStatus(true);
		customerRepository.save(customer);
		return ModelMapperConverter.map(customer, CustomerDto.class);

	}

	@Override
	public List<CustomerDto> listCustomers() {
		return ModelMapperConverter.mapAll(customerRepository.findByStatusTrue(), CustomerDto.class);
	}

	@Override
	public BankAccountDto getBankAccount(String accountID) throws AccountNotFountException {
		Optional<BankAccount> bankAccount = bankAccountRepository.findById(accountID);
		if (!bankAccount.isPresent()) {
			throw new AccountNotFountException("account not found");
		}
		if (bankAccount.get() instanceof CurrentAccount) {
			CurrentAccount currentAccount = (CurrentAccount) bankAccount.get();
			return ModelMapperConverter.map(currentAccount, CurrentAccountDto.class);
		} else {
			SavingAccount savingAccount = (SavingAccount) bankAccount.get();
			return ModelMapperConverter.map(savingAccount, SavingBankAccountDto.class);

		}

	}

	@Override
	public void debit(String accountId, double amount, String description)
			throws AccountNotFountException, BalanceNotSufficentException {
		BankAccount bankAccount = bankAccountRepository.findById(accountId)
				.orElseThrow(() -> new AccountNotFountException("BankAccount not found"));

		if (bankAccount.getBalance() < amount) {
			throw new BalanceNotSufficentException("Balance not sufficient");
		}

		AccountOperation accountOperation = new AccountOperation();
		accountOperation.setAmount(amount);
		accountOperation.setType(operationType.DEBIT);
		accountOperation.setOperationDate(new Date());
		accountOperation.setDesciption(description);
		accountOperation.setBankAccount(bankAccount);
		accountOperationRepository.save(accountOperation);
		bankAccount.setBalance((bankAccount).getBalance() - amount);
		bankAccountRepository.save(bankAccount);
	}

	@Override
	public void credit(String accountId, double amount, String description) throws AccountNotFountException {
		BankAccount bankAccount = bankAccountRepository.findById(accountId)
				.orElseThrow(() -> new AccountNotFountException("BankAccount not found"));
		if (accountId == null) {
			throw new IllegalArgumentException("accountId cannot be null");
		}
		AccountOperation accountOperation = new AccountOperation();
		accountOperation.setAmount(amount);
		accountOperation.setType(operationType.CREDIT);
		accountOperation.setOperationDate(new Date());
		accountOperation.setDesciption(description);
		accountOperation.setBankAccount(bankAccount);
		accountOperationRepository.save(accountOperation);
		bankAccount.setBalance((bankAccount).getBalance() + amount);
		bankAccountRepository.save(bankAccount);

	}

	@Override
	public CurrentAccountDto saveCurrentBankAccount(Long customerId, double intitalBalance, double overDraft)
			throws CustomerNotFoundException {
		CurrentAccount bankAccount = new CurrentAccount();
		Optional<Customer> customer = customerRepository.findById(customerId);
		if (customer.isPresent()) {
			bankAccount.setCustomer(customer.get());
		} else {
			throw new CustomerNotFoundException("customer not found");
		}

		bankAccount.setId(UUID.randomUUID().toString());
		bankAccount.setCreateDate(new Date());
		bankAccount.setBalance(intitalBalance);
		bankAccount.setOverDraft(overDraft);

		return ModelMapperConverter.map(bankAccountRepository.save(bankAccount), CurrentAccountDto.class);

	}

	@Override
	public SavingBankAccountDto saveSavingBankAccount(Long customerId, double intitalBalance, double interestRate)
			throws CustomerNotFoundException {
		SavingAccount bankAccount = new SavingAccount();
		Optional<Customer> customer = customerRepository.findById(customerId);
		if (customer.isPresent()) {
			bankAccount.setCustomer(customer.get());
		} else {
			throw new CustomerNotFoundException("customer not found");
		}

		bankAccount.setId(UUID.randomUUID().toString());
		bankAccount.setCreateDate(new Date());
		bankAccount.setBalance(intitalBalance);
		bankAccount.setInterestedRate(interestRate);
		bankAccountRepository.save(bankAccount);
		SavingBankAccountDto savingDto = ModelMapperConverter.map(bankAccount, SavingBankAccountDto.class);

		return savingDto;
	}

	@Override
	public void transfer(String intialIdAccount, String accountIdDestination, double amount)
			throws AccountNotFountException, BalanceNotSufficentException {
		debit(intialIdAccount, amount, "Transfer to " + accountIdDestination);
		credit(accountIdDestination, amount, "Transfer from " + intialIdAccount);

	}

	@Override
	public List<BankAccountDto> bankAountList() {

		List<BankAccount> bankAccounts = bankAccountRepository.findAll();

		List<BankAccountDto> bankAccountsDto = bankAccounts.stream().map(bankAccount -> {

			if (bankAccount instanceof CurrentAccount) {

				CurrentAccount currentAccount = (CurrentAccount) bankAccount;
				return ModelMapperConverter.map(currentAccount, CurrentAccountDto.class);

			} else {
				SavingAccount savingAccount = (SavingAccount) bankAccount;
				return ModelMapperConverter.map(savingAccount, SavingBankAccountDto.class);

			}
		}).collect(Collectors.toList());
		return bankAccountsDto;

	}

	@Override
	public CustomerDto getCustomer(Long customerId) throws CustomerNotFoundException {
		Optional<Customer> customer = customerRepository.findById(customerId);
		if (!customer.isPresent()) {
			throw new CustomerNotFoundException("Customer not fount");
		}
		return ModelMapperConverter.map(customer, CustomerDto.class);
	}

	@Override
	public void deleteCustomer(Long customerId) {
		Customer customer = customerRepository.findById(customerId).get();
		customer.setStatus(false);
		customerRepository.save(customer);

	}

	@Override
	public CustomerDto updateCustomer(Long customerId, CustomerDto newCustomerDto) {
		Optional<Customer> oldCustomer = customerRepository.findById(customerId);
		Customer newCust = ModelMapperConverter.map(newCustomerDto, Customer.class);
		if (oldCustomer.isPresent() && oldCustomer.get().getStatus() == true) {
			if (newCust.getFirstName() != null) {
				oldCustomer.get().setFirstName(newCust.getFirstName());
			}
			if (newCust.getLastName() != null) {
				oldCustomer.get().setLastName(newCust.getLastName());
			}
			if (newCust.getEmail() != null) {
				oldCustomer.get().setEmail(newCust.getEmail());
			}
			if (newCust.getAdress() != null) {
				oldCustomer.get().setAdress(newCust.getAdress());
			}
			if (newCust.getJob() != null) {
				oldCustomer.get().setJob(newCust.getJob());
			}
			if (newCust.getPhone() != null) {
				oldCustomer.get().setPhone(newCust.getPhone());
			}
			return ModelMapperConverter.map(customerRepository.save(oldCustomer.get()), CustomerDto.class);
		}
		return null;
	}

	@Override
	public List<AccountOperationDto> accountHistory(String id) {
		List<AccountOperation> accountOperation = accountOperationRepository.findByBankAccountId(id);

		return ModelMapperConverter.mapAll(accountOperation, AccountOperationDto.class);
	}

	@Override
	public Page<AccountOperationDto> getAllUsersPagination(String accountId, int page, int size) {
		return accountOperationRepository
				.findAll(PageRequest.of(page, size).withSort(Sort.by("operationDate").ascending()))
				.map(entity -> ModelMapperConverter.map(entity, AccountOperationDto.class));
	}

	@Override
	public Page<BankAccountDto> getAllAccountsPagination(int page, int size) {
		return bankAccountRepository.findAll(PageRequest.of(page, size).withSort(Sort.by("createDate")))
				.map(entity -> ModelMapperConverter.map(entity, BankAccountDto.class));
	}

	@Override
	public Page<CustomerDto> getAllCustomersPagination(int page, int size) {
		return customerRepository.findByStatusTrue(PageRequest.of(page, size).withSort(Sort.by("firstName")))
				.map(entity -> ModelMapperConverter.map(entity, CustomerDto.class));
	}

	@Override
	public Page<CustomerDto> rechercheUser(String key, int page, int size) {

		return (Page<CustomerDto>) customerRepository.rechercheUser(key, PageRequest.of(page, size))
				.map(entity -> ModelMapperConverter.map(entity, CustomerDto.class));
	}

	@Override
	public Page<BankAccountDto> rechercheAccount(String key, int page, int size) {
		return (Page<BankAccountDto>) bankAccountRepository
				.rechercheBankAccount(key, PageRequest.of(page, size).withSort(Sort.by("createDate")))
				.map(entity -> ModelMapperConverter.map(entity, BankAccountDto.class));

	}

	@Override
	public List<BankAccountDto> getAccountsByUserId(Long id) {

		return ModelMapperConverter.mapAll(bankAccountRepository.findByCustomerId(id), BankAccountDto.class);
	}

}
