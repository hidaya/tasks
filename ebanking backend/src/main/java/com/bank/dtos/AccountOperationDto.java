package com.bank.dtos;

import java.util.Date;

import com.bank.enums.operationType;

import lombok.Data;
@Data
public class AccountOperationDto {
	private Long id;
	private Date operationDate;
	private double amount;
	private operationType type;
	private String desciption;

}
