package com.bank.dtos;

import lombok.Data;

@Data
public class CreditDto {

	private double amount;
	private String description;
	private String accountId;
	
}
