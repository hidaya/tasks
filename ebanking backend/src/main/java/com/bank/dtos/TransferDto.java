package com.bank.dtos;

import lombok.Data;

@Data
public class TransferDto {

    private String accountId;
    private String accountDestination;
    private double amount;
    private String description;
    
}

	

