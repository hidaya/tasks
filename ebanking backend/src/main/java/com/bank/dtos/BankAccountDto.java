package com.bank.dtos;

import java.util.Date;
import java.util.List;

import com.bank.entities.AccountOperation;
import com.bank.enums.AccountStatus;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class BankAccountDto {



	private String id;

	private double balance;
	private Date createDate;

	private AccountStatus status;
	private String currency;
   private String type;
	private CustomerDto customer;
	private List<AccountOperationDto> operations;

}
