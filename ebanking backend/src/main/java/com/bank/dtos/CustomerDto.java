package com.bank.dtos;



import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class CustomerDto {
	private Long id;
	private String firstName;
	private String lastName;
	private String email;
	private String adress;
	private String phone;
	private String job;
	private String password;

}
