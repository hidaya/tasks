package com.bank.repositories;



import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bank.entities.BankAccount;


@Repository
public interface BankAccountRepository extends JpaRepository<BankAccount, String> {


	@Query("SELECT e FROM BankAccount e WHERE " +
		       "LOWER(e.id) LIKE CONCAT('%', :key, '%') " +
		       "OR CAST(e.balance AS string)LIKE CONCAT('%', LOWER(:key), '%')"+ 
		       "OR CAST(e.createDate AS string)LIKE CONCAT('%', LOWER(:key), '%')")
		Page<BankAccount> rechercheBankAccount(String key, Pageable pageable) ;

List<BankAccount>findByCustomerId(Long id);}