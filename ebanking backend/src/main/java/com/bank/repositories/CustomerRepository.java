package com.bank.repositories;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Repository;

import com.bank.entities.Customer;


@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

	@Query(value = "SELECT a FROM Customer a WHERE (a.firstName LIKE %?1% OR a.lastName LIKE %?1% OR a.phone LIKE %?1% OR a.email LIKE %?1% OR a.adress LIKE %?1% OR a.job LIKE %?1%) AND a.status = true")
	Page<Customer> rechercheUser(String key, Pageable pageable);

	Page<Customer> findByStatusTrue(Pageable pageable);

	List<Customer> findByStatusTrue();

	Optional<Customer> findByEmail(String email);

	Boolean existsByEmail(String email);

}