package com.bank.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bank.entities.AccountOperation;

@Repository
public interface AccountOperationRepository extends JpaRepository<AccountOperation, Long> {
List<AccountOperation> findByBankAccountId(String id);

}