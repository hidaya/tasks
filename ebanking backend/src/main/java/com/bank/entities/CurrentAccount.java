package com.bank.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
 @ToString
 @AllArgsConstructor
 @NoArgsConstructor
@DiscriminatorValue("CA")
public class CurrentAccount extends BankAccount{
private double overDraft;

}
