package com.hideya.controllers;

import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.hideya.dto.EmployeeDto;
import com.hideya.services.FileService;
import com.hideya.services.UserService;

import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/employee")
@RequiredArgsConstructor
public class EmployeeController {

	private final FileService fileService;
	private final UserService service;

	@PostMapping()
	public ResponseEntity<Object> postEmployee(@RequestBody EmployeeDto employee) {

		return ResponseEntity.ok().body(service.addEmployeee(employee));
	}

	@GetMapping("/{id}")
	public ResponseEntity<Object> getEmployeeById(@PathVariable Long id) throws EntityNotFoundException {

		return ResponseEntity.ok().body(service.getEmployeeById(id));

	}

	@DeleteMapping(value = "/{id}")

	public void supprimerAnnonce(@PathVariable long id){
		service.deleteEmployeeById(id);

	}

	@GetMapping()
	public ResponseEntity<Object> getAllEmployee() {

		return new ResponseEntity<>(service.findAllEmployee(), HttpStatus.OK);
	}

	@GetMapping("/pagination")
	public ResponseEntity<Object> getAllEmployeePagination(  @RequestParam(defaultValue = "0") int page,
			  @RequestParam(defaultValue = "2") int size) {
		Page<EmployeeDto> getAllUsersPaginate = service.getAllUsersPagination(page, size);
		if(getAllUsersPaginate.isEmpty()) {return new ResponseEntity<>("", HttpStatus.OK);}
		return new ResponseEntity<>(getAllUsersPaginate, HttpStatus.OK);
	}

	@GetMapping("/asc")
	public ResponseEntity<Object> getAllEmployeeAscByName() {

		return new ResponseEntity<>(service.findAllOrderAscByName(), HttpStatus.OK);
	}

	@GetMapping("/recherche")
	public ResponseEntity<Object> filterEmployeeByName(@RequestParam String key) {

		return new ResponseEntity<>(service.rechercheUserByName(key), HttpStatus.OK);
	}

	@PatchMapping("/{id}")
	public ResponseEntity<Object> putEmployee(@RequestBody EmployeeDto employee, @PathVariable Long id) {

		return ResponseEntity.ok().body(service.updateEmployee(employee, id));
	}

	@PutMapping("role/{id}")
	public void putEmployeeRole(@PathVariable Long id) throws EntityNotFoundException{

	service.updateRole(id);
	}

	@PutMapping("/profile")
	public ResponseEntity<Object> putEmployeProfile(@RequestParam(value = "id") Long id,
			@RequestParam(value = "picture") MultipartFile picture) throws Exception {

		return  ResponseEntity.ok().body(service.updatePicture(id, picture));
	}
	@GetMapping(value = "profile/{filename}", produces = MediaType.IMAGE_PNG_VALUE)
	@ResponseBody
	public ResponseEntity<Object> getFile(@PathVariable String filename) {
		Resource file = null;
		try {
			file = fileService.load(filename);
			if (file == null)
				return new ResponseEntity<>("CV non trouvé", HttpStatus.NO_CONTENT);
			
		} catch (Exception e) {
			System.out.println("hello");
		}

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + filename + "\"")
				.body(file);

	}

}