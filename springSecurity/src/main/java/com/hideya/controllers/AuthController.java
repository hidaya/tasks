package com.hideya.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hideya.dto.registerDto;
import com.hideya.model.Employee;
import com.hideya.services.UserService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/api/v1/auth/")
@RequiredArgsConstructor
public class AuthController {
	private final UserService service;

	@PostMapping("/login")
	public ResponseEntity<Object> authentification(@RequestBody Employee loginDto) {

		return ResponseEntity.ok().body(service.login(loginDto));
	}

	@PostMapping("/register")
	public ResponseEntity<Object> register(@RequestBody @Valid registerDto registerDto) {

		return new ResponseEntity<>(service.register(registerDto),HttpStatus.CREATED);
	}

}
