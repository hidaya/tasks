package com.hideya.dto;

import java.io.Serializable;
import java.util.List;

import com.hideya.model.Course;
import com.hideya.model.Role;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	private String email;

	private String name;

	private String phone;

	private String job;

	private String picture;
	
	private Role role;
	
	private List<Course> courses;
}
