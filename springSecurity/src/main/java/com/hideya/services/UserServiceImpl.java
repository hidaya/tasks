package com.hideya.services;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.modelmapper.internal.bytebuddy.utility.RandomString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.hideya.dto.EmployeeDto;
import com.hideya.dto.PictureDto;

import com.hideya.dto.registerDto;
import com.hideya.filter.AuthenticationResponse;
import com.hideya.filter.jwtService;
import com.hideya.helpers.ModelMapperConverter;
import com.hideya.model.Employee;
import com.hideya.model.Role;
import com.hideya.repository.EmployeeRepo;

import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service

public class UserServiceImpl implements UserService {
	private static final Logger LOGGER = LoggerFactory.getLogger(FileServiceImpl.class);
	private final jwtService jwtService;
	private final AuthenticationManager authenticationManager;
	private final EmployeeRepo Repo;
	private final PasswordEncoder encoder;
	private final FileService fileService;

	@Override
	public List<EmployeeDto> findAllEmployee() {

		return ModelMapperConverter.mapAll(Repo.findAll(), EmployeeDto.class);
	}

	@Override
	public EmployeeDto addEmployeee(EmployeeDto employeedTO) {

		Employee employee = ModelMapperConverter.map(employeedTO, Employee.class);
		Repo.save(employee);
		return ModelMapperConverter.map(employee, EmployeeDto.class);

	}

	@Override
	public EmployeeDto updateEmployee(EmployeeDto employeeDto, Long id) {

		Optional<Employee> userOpt = Repo.findById(id);
		if (!userOpt.isPresent()) {

			return null;

		}
		Employee user = userOpt.get();

		if (employeeDto.getEmail() != null)
			user.setEmail(user.getEmail());
		if (employeeDto.getJob() != null)
			user.setJob(employeeDto.getJob());
		if (employeeDto.getName() != null)
			user.setName(employeeDto.getName());
		if (employeeDto.getPhone() != null)
			user.setPhone(employeeDto.getPhone());

		Repo.save(user);
		return ModelMapperConverter.map(user, EmployeeDto.class);

	}

	@Override
	public EmployeeDto getEmployeeById(Long id) throws EntityNotFoundException {
		Optional<Employee> emp = Repo.findById(id);
		if (!emp.isPresent())
			throw new EntityNotFoundException("USER NOT FOUND IN BD");
		return ModelMapperConverter.map(emp, EmployeeDto.class);
	}

	@Override
	public void deleteEmployeeById(Long id) {
		Repo.deleteById(id);

		LOGGER.info("deleted");
	}

	@Override

	public AuthenticationResponse login(Employee login) {

		Authentication authentication = authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(login.getEmail(), login.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);

		UserDetails userDetails = (UserDetails) authentication.getPrincipal();

		// Ajout de l'id de l'utilisateur comme claim supplémentaire dans le token
		Employee employee = Repo.findByEmail(userDetails.getUsername()).orElseThrow(
				() -> new UsernameNotFoundException("User not found with email: " + userDetails.getUsername()));
		Long userId = employee.getId();
		Map<String, Object> extraClaims = new HashMap<>();
		extraClaims.put("id", userId);

		String jwt = jwtService.generateToken(extraClaims, userDetails);
		return AuthenticationResponse.builder().token(jwt).build();
	}

	@Override
	public registerDto register(registerDto request) {

		Employee user = new Employee();
		user.setName(request.getName());

		user.setEmail(request.getEmail());
		user.setPassword(encoder.encode(request.getPassword()));
		user.setRole(Role.USER);
		user.setCode(RandomString.make(64));
		var savedUser = Repo.save(user);
		return ModelMapperConverter.map(savedUser, registerDto.class);
	}

	@Override
	public List<EmployeeDto> findAllOrderAscByName() {
		return ModelMapperConverter.mapAll(Repo.listUser(), EmployeeDto.class);
	}

	@Override
	public List<EmployeeDto> rechercheUserByName(String key) {
		return ModelMapperConverter.mapAll(Repo.rechercheUser(key), EmployeeDto.class);

	}

	@Override
	public void updateRole(Long id) throws EntityNotFoundException {
		Optional<Employee> emp = Repo.findById(id);
		if (!emp.isPresent())
			throw new EntityNotFoundException("USER NOT FOUND IN BD");
		if (emp.get().getRole() != Role.ADMIN)
			emp.get().setRole(Role.ADMIN);
		Repo.save(emp.get());

	}

	@Override
	public PictureDto updatePicture(Long id, MultipartFile userPicture) throws Exception {

		Optional<Employee> user = Repo.findById(id);
		if (!user.isPresent())
			throw new EntityNotFoundException("USER NOT FOUND IN BD");

		String pictureName = fileService.save(userPicture);
		user.get().setPicture(pictureName);
		Employee savedUser = Repo.save(user.get());

		PictureDto picture = ModelMapperConverter.map(savedUser, PictureDto.class);
		picture.setPicture(savedUser.getPicture());

		return picture;

	}

	@Override
	public Page<EmployeeDto> getAllUsersPagination(int page, int size) {
		return Repo.findAll(PageRequest.of(page, size).withSort(Sort.by("name")))
				.map(entity -> ModelMapperConverter.map(entity, EmployeeDto.class));

	}

	Logger log = LoggerFactory.getLogger(UserService.class);

	/*
	 * //schedule a job to add object in DB (Every 15 sec)
	 * 
	 * @Scheduled(fixedRate = 15000) public void addDBJob() { Employee user = new
	 * Employee(); user.setName("user" + new Random().nextInt(374483));
	 * Repo.save(user); System.out.println("add service call in " + new
	 * Date().toString()); }
	 */
	@Scheduled(cron = "0 03 14 22 * ?")
	// 10h13 le 21 de chaque mois("sec min h j mois)
	public void fetchDBJob() {
		List<Employee> users = Repo.findAll();
		for (Employee employee : users) {

			if (employee.getJob().equals("ingenieur") && employee.getExperience() <= 2)
				employee.setSalary(2200.000);
			Repo.save(employee);
		}

		System.out.println("fetch service call in " + new Date().toString());
		System.out.println("nombre of record fetched : " + users.size());
		log.info("users : {}", users);
	}

}
