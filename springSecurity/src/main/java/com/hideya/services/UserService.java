package com.hideya.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import com.hideya.dto.EmployeeDto;
import com.hideya.dto.PictureDto;
import com.hideya.dto.registerDto;
import com.hideya.exception.UsernotFound;
import com.hideya.filter.AuthenticationResponse;
import com.hideya.model.Employee;

import jakarta.persistence.EntityNotFoundException;

public interface UserService {

	List<EmployeeDto> findAllEmployee();

	EmployeeDto addEmployeee(EmployeeDto employee);

	EmployeeDto updateEmployee(EmployeeDto employee, Long id);

	EmployeeDto getEmployeeById(Long id) throws  EntityNotFoundException;

	void deleteEmployeeById(Long id);

	AuthenticationResponse login(Employee loginDto);

	registerDto register(registerDto registerDto);

	List<EmployeeDto> findAllOrderAscByName();

	List<EmployeeDto> rechercheUserByName(String key);

	void updateRole(Long id);

	PictureDto updatePicture(Long id, MultipartFile userPicture) throws  Exception;

	Page<EmployeeDto> getAllUsersPagination(int page, int size);
	
}
