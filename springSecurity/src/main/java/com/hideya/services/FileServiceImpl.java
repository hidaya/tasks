package com.hideya.services;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.hideya.exception.InvalidEntityException;

@Service
public class FileServiceImpl implements FileService {

	private static final Logger LOGGER = LoggerFactory.getLogger(FileServiceImpl.class);
	private static final String UPLOADS_DIR_NAME = "uploads";
	private static final long MAX_UPLOAD_SIZE = 10485760; // 10 Mo
	private static final List<String> ACCEPTED_IMAGE_TYPES = Arrays.asList("image/jpeg", "image/png");

	private String userDirectory = System.getProperty("user.home");

	private final Path root = Paths.get(userDirectory, UPLOADS_DIR_NAME);

	@Override
	public String save(MultipartFile file) throws Exception {
		LOGGER.info("Uploading file: {}", file.getOriginalFilename());
		if (file.isEmpty()) {
			throw new Exception("Le fichier est vide.");
		}
		if (!ACCEPTED_IMAGE_TYPES.contains(file.getContentType())) {
			throw new Exception("Le fichier doit être une image.");
		}
		if (file.getSize() > MAX_UPLOAD_SIZE) {
			throw new Exception("Le fichier est trop volumineux.");
		}
		try {
			if (!Files.exists(root)) {
				Files.createDirectories(root);
			}
			String filename = StringUtils.cleanPath(file.getOriginalFilename());
			filename = String.valueOf(new Date().getTime()).concat(filename.toLowerCase());
			try (InputStream inputStream = file.getInputStream()) {
				Files.copy(inputStream, this.root.resolve(filename), StandardCopyOption.REPLACE_EXISTING);
			}
			return filename;
		} catch (Exception e) {
			throw new Exception("Impossible de copier le fichier");
		}
	}

	@Override
	
	    public Resource load(String filename) {
	        LOGGER.info("Loading file: {}", filename);
	        try {
	            Path file = root.resolve(filename);
	            Resource resource = new UrlResource(file.toUri());
	            if (resource.exists() || resource.isReadable())
	                return resource;

	        } catch (MalformedURLException e) {
	            throw new InvalidEntityException("Erreur lors du chargement du fichier");
	        }
	        return null;
	    }
}
