package com.hideya.services;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;



public interface FileService {

	String save(MultipartFile file) throws Exception;

	Resource load(String filename) throws Exception;

}
