package com.hideya.filter;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.hideya.repository.EmployeeRepo;

import lombok.RequiredArgsConstructor;
//injecter les beans dans l'application
@Configuration
@RequiredArgsConstructor
public class ApplicationConfig {
	private final EmployeeRepo repository;
	  @Bean
	  public UserDetailsService userDetailsService() {
	   return new UserDetailsService() {
		
		   //avoir le user de la bd
		@Override
		public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

			return repository.findByEmail(username).orElseThrow(() -> new UsernameNotFoundException("User not found"));
		}
	};
	  }

	  @Bean
	  public AuthenticationProvider authenticationProvider() {
	    DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
	    authProvider.setUserDetailsService(userDetailsService());
	    authProvider.setPasswordEncoder(passwordEncoder());
	    return authProvider;
	  }

	  @Bean
	  public AuthenticationManager authenticationManager(AuthenticationConfiguration config) throws Exception {
	    return config.getAuthenticationManager();
	  }

	  @Bean
	  public PasswordEncoder passwordEncoder() {
	    return new BCryptPasswordEncoder();
	  }
}
