package com.hideya.filter;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;

@Service
public class jwtService {

	//https://www.allkeysgenerator.com/ger=generer code plus efficace>256 :pour le decryptage
	private static final String SECRET_KEY ="404E635266556A586E3272357538782F413F4428472B4B6250645367566B5970";
	
	
	 //transformer le key générer de byte en key
	private Key getSignInKey() {
		  byte[] keyBytes = Decoders.BASE64.decode(SECRET_KEY);
		    return Keys.hmacShaKeyFor(keyBytes);
	}
	
	
	//entraire le body du payload tu token
	 private Claims extractAllClaims(String token) {
		    return Jwts
		        .parserBuilder()
		        .setSigningKey(getSignInKey())
		        .build()
		        .parseClaimsJws(token)
		        .getBody();
		  }
	 
	 
	 //extraire  un claim de la fonction extract all clamis qlq soit sont type 
	  public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
		    final Claims claims = extractAllClaims(token);
		    return claimsResolver.apply(claims);
		  }
	 
//extraire le username qui est dans le claim getSubject   (subject=email or username)
	  public String extractUsername(String token) {
		    return extractClaim(token, Claims::getSubject);
		  }

	 //generer le token 
	  public String generateToken(UserDetails userDetails) {
		    return generateToken(new HashMap<>(), userDetails);
		  }
	  //generer token sans extraclaims
		  public String generateToken(
		      Map<String, Object> extraClaims,
		      UserDetails userDetails
		  ) {
		    return Jwts
		        .builder()
		        .setClaims(extraClaims)
		        .setSubject(userDetails.getUsername())
		        .claim("roles", userDetails.getAuthorities())
		        .setIssuedAt(new Date(System.currentTimeMillis()))
		        .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 24))
		        .signWith(getSignInKey(), SignatureAlgorithm.HS256)
		        .compact();
		  }
	  
	
		  
		  //camparer le username dans le token  parraport au username 
		  public boolean isTokenValid(String token, UserDetails userDetails) {
			    final String username = extractUsername(token);
			    return (username.equals(userDetails.getUsername())) && !isTokenExpired(token);
			  }

//calculer la date d'envoie token par rapport exipartion date of token
		  private boolean isTokenExpired(String token) {
			    return extractExpiration(token).before(new Date());
			  }

//extraire la date d'expiration token de la token
		  private Date extractExpiration(String token) {
			    return extractClaim(token, Claims::getExpiration);
			  }

}
