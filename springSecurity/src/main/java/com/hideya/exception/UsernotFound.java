package com.hideya.exception;

public class UsernotFound extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	 public UsernotFound(String message) {
	        super(message);
	    }
}
