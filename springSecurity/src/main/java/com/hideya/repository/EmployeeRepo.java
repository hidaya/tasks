package com.hideya.repository;


import java.util.List;
import java.util.Optional;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hideya.model.Employee;

@Repository
public interface EmployeeRepo extends JpaRepository<Employee, Long> {
	Optional<Employee> findByEmail(String email);
	@Query(value = "SELECT a FROM Employee a ORDER BY a.name ASC ")
	List<Employee> listUser();

	@Query(value = "SELECT a FROM Employee a WHERE a.name  LIKE %?1% " + "Or a.code LIKE %?1%" + "Or a.job LIKE %?1%"
			+ "Or a.email LIKE %?1%")
	List<Employee> rechercheUser(String keyword);

}
