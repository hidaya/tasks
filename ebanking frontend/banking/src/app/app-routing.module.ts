import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NavbarComponent } from './navbar/navbar.component';
import { CustomersComponent } from './customers/customers.component';
import { AccountsComponent } from './accounts/accounts.component';
import { TranferComponent } from './tranfer/tranfer.component';
import { SaveAccountComponent } from './save-account/save-account.component';
import { LoginComponent } from './login/login.component';
import { AuthGuardGuard } from './guards/auth-guard.guard';
import { RoleGuardGuard } from './guards/role-guard.guard';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {
    path: 'customers',
    component: CustomersComponent,
    canActivate: [RoleGuardGuard, AuthGuardGuard],
    data: {
      roles: 'ADMIN',
    },
  },
  {
    path: 'accounts',
    component: AccountsComponent,
    canActivate: [RoleGuardGuard, AuthGuardGuard],
    data: {
      roles: 'ADMIN',
    },
  },
  {
    path: 'operations',
    component: TranferComponent,
    canActivate: [RoleGuardGuard, AuthGuardGuard],
    data: {
      roles: 'USER',
    },
  },

  {
    path: 'login',
    component: LoginComponent,
  },  {
    path: '',
    component: HomeComponent,
  },
  // {
  //   path: 'apprenti/cursus/:id',
  //   component: TestComponent,
  //   children: [
  //     {
  //       path: 'lesson/:id',
  //       component: LessonComponent,
  //     },
  //   ],
  //   canActivate: [AuthGuardGuard],

  // },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
