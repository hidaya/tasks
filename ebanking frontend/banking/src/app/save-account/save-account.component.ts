import { Component, Inject, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AccountsService } from '../services/accounts.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-save-account',
  templateUrl: './save-account.component.html',
  styleUrls: ['./save-account.component.scss'],
})
export class SaveAccountComponent implements OnInit {
  customerForm!: FormGroup;
  userId!: any;
  customer: any;
  constructor(
    private dialogRef: MatDialogRef<SaveAccountComponent>,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) public editData: any,
    private toastr: ToastrService,
    private accountService: AccountsService,
    private formBuilder: FormBuilder
  ) {}
  ngOnInit(): void {
    this.customer = this.editData;
    console.log(this.customer);

    this.customerForm = this.formBuilder.group({
      type: new FormControl('', [Validators.required]),
      balance: new FormControl('', [Validators.required]),
      overDraft: new FormControl('', [Validators.required]),
      interestRate: new FormControl('', [Validators.required]),
      customer: new FormControl('', [Validators.required]),
    });

    if (this.editData) {
      this.customerForm.patchValue({
        customer: this.editData,
      });
    }
  }
  createAccount() {
    let type = this.customerForm.value.type;
    this.customerForm.value.customer = this.customer;
    console.log(this.customerForm.value);

    if (type == 'COURANT') {
      this.accountService.SaveCUrrentAccount(this.customerForm.value).subscribe(
        (data) => {
          if (data != null) {
            console.log(data);
            this.customerForm.reset();
            this.toastr.success('Compte ajouté avec succès !');
          }
        },
        (error) => {
          if (error != null) {
            if (error.status === 400) {
              this.toastr.error('Répéter avec des données valide !');
            } else {
              this.toastr.error(
                'Une erreur est survenue, réessayez plus tard !'
              );
            }
          }
        }
      );
    } else if (type == 'EPARGNE') {
      this.accountService.SaveSavingAccount(this.customerForm.value).subscribe(
        (data) => {
          if (data != null) {
            console.log(data);
            this.customerForm.reset();
            this.toastr.success('Compte ajouté avec succès !');
          }
        },
        (error) => {
          if (error != null) {
            if (error.status === 400) {
              this.toastr.error('Répéter avec des données valide !');
            } else {
              this.toastr.error(
                'Une erreur est survenue, réessayez plus tard !'
              );
            }
          }
        }
      );
    }
  }
}
