import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { AccountsService } from '../services/accounts.service';

@Component({
  selector: 'app-account-details',
  templateUrl: './account-details.component.html',
  styleUrls: ['./account-details.component.scss'],
})
export class AccountDetailsComponent implements OnInit {
  displayedColumns: string[] = [
    'operationDate',
    'amount',
    'type',
    'description',
  ];
  id!: number;
  dataSource: any;
  x!: number;
  constructor(
    private toastr: ToastrService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<AccountDetailsComponent>,
    private accountService: AccountsService
  ) {}

  ngOnInit(): void {
    this.id = this.data.id;
    console.log(this.id);
    this.getCustomerDetails();
  }

  getCustomerDetails() {
    this.accountService.getAccount(this.id).subscribe(
      (data) => {
        this.dataSource = data.operations;
        console.log(this.dataSource);
        this.x = this.dataSource.length;
      },
      (error) => {
        if (error != null) {
          if (error.status === 400) {
            this.toastr.error('Répéter votre suppression !');
          } else {
            this.toastr.error('Une erreur est survenue, réessayez plus tard !');
          }
        }
      }
    );
  }
}
