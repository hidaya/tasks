import { Component, Inject, OnInit } from '@angular/core';
import { CustomersService } from '../services/customers.service';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatDialog,
} from '@angular/material/dialog';
import { DeleteCustomerComponent } from '../delete-customer/delete-customer.component';
import { EditCustomerComponent } from '../edit-customer/edit-customer.component';
import { map } from 'rxjs';
import { EditCustomerDetailsComponent } from '../edit-customer-details/edit-customer-details.component';
import { SaveAccountComponent } from '../save-account/save-account.component';
import { customer } from '../models/customerModel';
@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss'],
})
export class CustomersComponent implements OnInit {
  constructor(
    private customerService: CustomersService,

    public dialog: MatDialog
  ) {}
  value: any = null;
  pageEvent!: PageEvent;
  counter = 1;
  sizeNumber!: number ;
  pageNumber!: number ;
  dataSource!: any;
  displayedColumns: string[] = [
    'firstName',
    'lastName',
    'email',
    'adress',
    'phone',
    'job',
    'action',
  ];
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  ngOnInit(): void {
    this.pageNumber = 0;
    this.sizeNumber = 6;
    this. getAllCustomers(0, 6)
  }

  getAllCustomers(page: number, size: number) {
    this.customerService
      .getAllCustomers(page, size)
      .subscribe((data) => (this.dataSource = data));
  }
  onPaginateChange(event: PageEvent) {
    this.pageNumber = event.pageIndex;
    this.sizeNumber = event.pageSize;
    if (!this.value) {
      this.customerService
        .getAllCustomers(this.pageNumber, this.sizeNumber)
        .subscribe((data) => (this.dataSource = data));
    } else {
      this. searchCust(this.value);
    }
  }

  openDialog(element: any): void {
    const dialogRef = this.dialog.open(DeleteCustomerComponent, {
      data: {
        firstName: element.firstName,
        lastName: element.lastName,
        id: element.id,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      // Rafraîchir le tableau des clients
      this.getAllCustomers(this.pageNumber, this.sizeNumber);
    });
  }
  openDialog2() {
    this.dialog
      .open(EditCustomerComponent, {
        data: {
          animal: 'panda',
        },
      })
      .afterClosed()
      .subscribe();
  }
  searchCust(key: string) {
    this.customerService
      .searchCustomer(key, this.pageNumber, this.sizeNumber)
      .subscribe((data) => {
        this.dataSource = data;
      });
  }
  openDialog3(id: number) {
    this.dialog
      .open(EditCustomerDetailsComponent, {
        data: id,
      })

      .afterClosed()
      .subscribe();
  }
  openDialog4(customer:any) {
    this.dialog
      .open(SaveAccountComponent, {
        data: customer,
      })

      .afterClosed()
      .subscribe();
  }
}
