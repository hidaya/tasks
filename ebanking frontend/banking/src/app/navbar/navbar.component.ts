import { Component, OnInit } from '@angular/core';
import { MatMenuPanel } from '@angular/material/menu/menu-panel';
import { Router } from '@angular/router';
import { TokenService } from '../token.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  isLoggedIn!: boolean;
  role: any;
  constructor(private route: Router, private tokenService: TokenService) {}
  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenService.isAuthenticated();
    this.tokenService.getRoles().subscribe((data) => {
      this.role = data;
    }).unsubscribe()
    console.log(this.role);
  }
  logout() {
    if (localStorage.getItem('token')) localStorage.clear();
    this.route.navigateByUrl('/login');
    this.isLoggedIn = false;
  }
}
