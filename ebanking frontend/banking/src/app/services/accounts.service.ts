import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, map, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AccountsService {
  constructor(private http: HttpClient) {}

  getAlllAccountsPaginate(page: number, size: number): Observable<any> {
    let params = new HttpParams();

    params = params.append('page', page);
    params = params.append('size', size);
    return this.http
      .get<any>(`http://localhost:8080/accounts/pageAccounts`, {
        params,
      })
      .pipe(
        map((appData: any) => appData),

        catchError((err) => throwError(err))
      );
  }

  searchAccount(key: string, page: number, size: number): Observable<any> {
    let params = new HttpParams();

    params = params.append('key', key);
    params = params.append('page', page);
    params = params.append('size', size);
    return this.http
      .get<any>(`http://localhost:8080/accounts/search`, {
        params,
      })
      .pipe(
        map((data: any) => data),

        catchError((err) => throwError(err))
      );
  }

  accountHistory(accountId: any): Observable<any> {
    return this.http.get<any>(
      `http://localhost:8080/accounts/pagehistory/${accountId}`
    );
  }
  getAccount(id: any): Observable<any> {
    return this.http
      .get<any>(`http://localhost:8080/accounts/${id}`)
      .pipe(map((res) => res));
  }
  transfer(data: any): Observable<any> {
    return this.http
      .post<any>(`http://localhost:8080/accounts/transfer`, data)
      .pipe(map((res) => res));
  }
  credit(data: any): Observable<any> {
    return this.http
      .post<any>(`http://localhost:8080/accounts/credit`, data)
      .pipe(map((res) => res));
  }
  debit(data: any): Observable<any> {
    return this.http
      .post<any>(`http://localhost:8080/accounts/debit`, data)
      .pipe(map((res) => res));
  }
  SaveCUrrentAccount(data: any): Observable<any> {
    return this.http
      .post<any>(`http://localhost:8080/accounts/savingCurrent`, data)
      .pipe(map((res) => res));
  }
  SaveSavingAccount(data: any): Observable<any> {
    return this.http
      .post<any>(`http://localhost:8080/accounts/savingSaving`, data)
      .pipe(map((res) => res));
  }
  getAccountByUser(id: any): Observable<any> {
    return this.http
      .get<any>(`http://localhost:8080/accounts/customer/${id}`)
      .pipe(map((res) => res));
  }
}
