import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable, throwError } from 'rxjs';
import { customer } from '../models/customerModel';
@Injectable({
  providedIn: 'root',
})
export class CustomersService {
  constructor(private http: HttpClient) {}

  getCustomers(): Observable<any> {
    return this.http.get<customer[]>('http://localhost:8080/customers');
  }

  getCustomerId(id: any): Observable<any> {
    return this.http.get<any>(`http://localhost:8080/customers/${id}`);
  }
  deleteCustomer(id: any): Observable<any> {
    return this.http.delete<any>(`http://localhost:8080/customers/${id}`);
  }

  editCustomer(User: any): Observable<any> {
    return this.http.put<any>(
      `http://localhost:8080/customers/${User.id}`,
      User
    );
  }

  addCustomer(data: any): Observable<any> {
    return this.http
      .post<any>(`http://localhost:8080/customers`, data)
      .pipe(map((res) => res));
  }
  searchCustomer(key: string, page: number, size: number): Observable<any> {
    let params = new HttpParams();

    params = params.append('key', key);
    params = params.append('page', page);
    params = params.append('size', size);
    return this.http
      .get<any>(`http://localhost:8080/customers/search`, {
        params,
      })
      .pipe(
        map((data: any) => data),

        catchError((err) => throwError(err))
      );
  }
  getAllCustomers(
    page: number,

    size: number
  ): Observable<any> {
    let params = new HttpParams();

    params = params.append('page', page);

    params = params.append('size', size);

    return this.http

      .get(`http://localhost:8080/customers/page`, {
        params,
      })

      .pipe(
        map((appData: any) => appData),

        catchError((err) => throwError(err))
      );
  }
}
