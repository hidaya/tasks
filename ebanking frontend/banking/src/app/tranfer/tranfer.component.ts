import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { AccountsService } from '../services/accounts.service';
import { ToastrService } from 'ngx-toastr';
import { MatExpansionModule } from '@angular/material/expansion';

@Component({
  selector: 'app-tranfer',
  templateUrl: './tranfer.component.html',
  styleUrls: ['./tranfer.component.scss'],
})
export class TranferComponent implements OnInit {
  standalone!: true;
  hoveredAccountId!:any;
  panelOpenState = false;
  customerForm!: FormGroup;
  isInputDisabled: boolean = true;
  accountFormGroup!: FormGroup;
  accountId!: string;
  checked = false;
  indeterminate = false;
  labelPosition: 'before' | 'after' = 'after';
  disabled = false;
  userId: any;
  account!: string;
  selectedAccountId!: string;

previousAccountId!: string;
  myAccounts: any[] = [];
  constructor(
    private toastr: ToastrService,
    private accountService: AccountsService,
    private formBuilder: FormBuilder
  ) {}
  ngOnInit(): void {
    this.userId = localStorage.getItem('user_id');
    this.accountService.getAccountByUser(this.userId).subscribe((data) => {
      this.myAccounts = data;
    });
    this.accountFormGroup = this.formBuilder.group({
      accountId: new FormControl({ value: '', disabled: true }),
    });
    this.customerForm = this.formBuilder.group({
      typeOperation: new FormControl('', [Validators.required]),
      amount: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      destination: new FormControl('', [
        Validators.required,
        Validators.minLength(36),
        Validators.maxLength(36),
      ]),
    });
  }
  selectAccount(accountId: string) {
    this.previousAccountId = this.selectedAccountId;
    this.selectedAccountId = accountId;
    console.log(this.selectedAccountId);

  }
  
  close() {
    this.customerForm.reset();
  }

  handleAccountOperation() {
    this.customerForm.value.accountId = this.selectedAccountId;

    let typeOperation = this.customerForm.value.typeOperation;
    console.log(this.customerForm.value.accountId);
    console.log(typeOperation);

    if (typeOperation == 'DEBIT') {
      this.accountService.debit(this.customerForm.value).subscribe(
        (data) => {
          if (data != null) {
            console.log(data);
            this.customerForm.reset();
            this.toastr.success('Opération faite avec succés!');
          }
        },
        (error) => {
          if (error != null) {
            if (error.status === 400) {
              this.toastr.error('Répéter votre suppression !');
            } else {
              this.toastr.error(
                'Une erreur est survenue, réessayez plus tard !'
              );
            }
          }
        }
      );
    } else if (typeOperation == 'CREDIT') {
      this.accountService.credit(this.customerForm.value).subscribe(
        (data) => {
          if (data != null) {
            console.log(data);
            this.customerForm.reset();
            this.toastr.success('Opération faite avec succés!');
          }
        },
        (error) => {
          if (error != null) {
            if (error.status === 400) {
              this.toastr.error('Répéter votre suppression !');
            } else {
              this.toastr.error(
                'Une erreur est survenue, réessayez plus tard !'
              );
            }
          }
        }
      );
    } else if (typeOperation == 'TRANSFER') {
      this.accountService.transfer(this.customerForm.value).subscribe(
        (data) => {
          if (data != null) {
            console.log(data);
            this.customerForm.reset();
            this.toastr.success('Opération faite avec succés!');
          }
        },
        (error) => {
          if (error != null) {
            if (error.status === 400) {
              this.toastr.error('Répéter votre suppression !');
            } else {
              this.toastr.error(
                'Une erreur est survenue, réessayez plus tard !'
              );
            }
          }
        }
      );
    }
  }
}
