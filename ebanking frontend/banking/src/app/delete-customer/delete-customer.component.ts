import { Component, OnInit, Inject } from '@angular/core';
import {
  MatDialog,
  MAT_DIALOG_DATA,
  MatDialogRef,
} from '@angular/material/dialog';
import { CustomersComponent } from '../customers/customers.component';
import { CustomersService } from '../services/customers.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-delete-customer',
  templateUrl: './delete-customer.component.html',
  styleUrls: ['./delete-customer.component.scss'],
})
export class DeleteCustomerComponent implements OnInit {
  application!: any;
  id: number;
  firstName:string;
  lastName:string;
  constructor(
    private toastr: ToastrService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<DeleteCustomerComponent>,
    private customerService: CustomersService
  ) {
    this.id = data.id;

    this.firstName=data.firstName;
    this.lastName=data.lastName;
    console.log(this.id,this.firstName,this.lastName);
  }

  ngOnInit(): void {
    this.application = this.data;
  }

  deleteCustomer(id: number) {
    this.customerService.deleteCustomer(id).subscribe(
      (data) => {
        console.log('customer deleted');
        this.dialogRef.close('save');
        this.toastr.success('Client supprimé avec succés!!');
      },
      (error) => {
        if (error != null) {
          if (error.status === 400) {
            this.toastr.error('Répéter votre suppression !');
          } else {
            this.toastr.error('Une erreur est survenue, réessayez plus tard !');
          }
        }
      }
    );
  }
}
