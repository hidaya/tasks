import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { TokenService } from '../token.service';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root',
})
export class RoleGuardGuard implements CanActivate {
  constructor(private tokenService: TokenService, private router: Router) {}
  private jwtHelper = new JwtHelperService();
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean | UrlTree {
    const token = this.tokenService.loadAccessToken();

    if (token) {
      const decodedToken = this.jwtHelper.decodeToken(token);
      const userRole = decodedToken.roles[0].authority;
      console.log(userRole);

      // Vérifier si le rôle utilisateur correspond aux rôles requis pour la route
      if (route.data['roles'].includes(userRole)) {
        return true; // L'utilisateur a le rôle requis
      } else {
        // L'utilisateur n'a pas le rôle requis
        return this.router.parseUrl('/');
      }
    }

    // Si aucun token n'est présent
    return this.router.parseUrl('/login');
  }
}
