import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { TokenService } from '../token.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm!: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private tokenService:TokenService,
    private toastr: ToastrService,
  ) {}

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [
        Validators.required,
        Validators.pattern(
          /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@$!%*#?&^_-]).{8,}/
        ),
      ]),
    });
  }
  login(): void {
    this.authService.login(this.loginForm.value).subscribe((response: any) => {
      const token = response.token;
      const decodedToken = JSON.parse(atob(token.split('.')[1])); // décoder le token et extraire les informations
      console.log(decodedToken);

      const userId = decodedToken.id;
      const userEmail = decodedToken.sub;
      const userRole = decodedToken.roles[0].authority;

      localStorage.setItem('token', token);
      localStorage.setItem('user_id', userId.toString());
      localStorage.setItem('user_email', userEmail);
      localStorage.setItem('user_role', userRole);
      console.log(userId, userEmail, userRole);

      if (userRole === 'USER') {
        this.router.navigateByUrl('/operations');
      } else if (userRole === 'ADMIN') {
        this.router.navigateByUrl('/customers');
      }
    },  (error) => {
      if (error != null) {
        if (error.status === 500) {
          this.toastr.error('Une erreur est survenue, réessayez plus tard !')
    
        } else {
          this.toastr.error('Taper des données valides !');
        }
      }
    });
  }
}
