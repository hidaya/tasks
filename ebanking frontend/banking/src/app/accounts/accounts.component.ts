import { Component, OnInit } from '@angular/core';
import { AccountsService } from '../services/accounts.service';
import { AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { AccountDetailsComponent } from '../account-details/account-details.component';
@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.scss'],
})
export class AccountsComponent implements OnInit {
  constructor(
    private accountService: AccountsService,
    public dialog: MatDialog
  ) {}
  pageNumber!: number;
  sizeNumber!: number;
  dataSource: any;

  value: any = null;
  pageEvent!: PageEvent;
  displayedColumns: string[] = [
    'id',
    'balance',
    'createDate',
    'type',
    'customer',
    'details',
  ];
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  ngOnInit(): void {
    this.pageNumber = 0;
    this.sizeNumber = 6;
    this.getAlllAccounts(0, 6);
  }

  getAlllAccounts(page: number, size: number) {
    this.accountService
      .getAlllAccountsPaginate(page, size)
      .subscribe((data) => {
        this.dataSource = data;
        console.log(this.dataSource);
        console.log('dfgh');
      });
  }
  onPaginateChange(event: PageEvent) {
    this.pageNumber = event.pageIndex;
    this.sizeNumber = event.pageSize;
    if (!this.value) {
      this.accountService
        .getAlllAccountsPaginate(this.pageNumber, this.sizeNumber)
        .subscribe((data) => (this.dataSource = data));
    } else {
      this.searchAccount(this.value);
    }
  }
  searchAccount(key: any) {
    this.accountService
      .searchAccount(key, this.pageNumber, this.sizeNumber)
      .subscribe((data) => {
        this.dataSource = data;
      });
    console.log(key);
  }
  openDialog3(id: number) {
    this.dialog
      .open(AccountDetailsComponent, {
        data: id,
      })

      .afterClosed()
      .subscribe();
  }
}
