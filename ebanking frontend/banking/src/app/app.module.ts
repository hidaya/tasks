import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MatButtonModule } from '@angular/material/button';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { NavbarComponent } from './navbar/navbar.component';
import { MatTreeModule } from '@angular/material/tree';
import { MatMenuModule } from '@angular/material/menu';
import { CustomersComponent } from './customers/customers.component';
import { AccountsComponent } from './accounts/accounts.component';
import { HttpClientModule } from '@angular/common/http';
import { MatPaginatorIntl } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatDialogModule } from '@angular/material/dialog';
import { DeleteCustomerComponent } from './delete-customer/delete-customer.component';
import { EditCustomerComponent } from './edit-customer/edit-customer.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MaterialModule } from './services/material-module/material-module.module';
import { ToastrModule } from 'ngx-toastr';
import { EditCustomerDetailsComponent } from './edit-customer-details/edit-customer-details.component';
import { AccountDetailsComponent } from './account-details/account-details.component';
import { TranferComponent } from './tranfer/tranfer.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { SaveAccountComponent } from './save-account/save-account.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { MatExpansionModule } from '@angular/material/expansion';
import { HomeComponent } from './home/home.component';
@NgModule({
  declarations: [
    AppComponent,

    CustomersComponent,
    AccountsComponent,
    DeleteCustomerComponent,
    EditCustomerComponent,
    EditCustomerDetailsComponent,
    AccountDetailsComponent,
    TranferComponent,
    SaveAccountComponent,
    LoginComponent,
    RegisterComponent,
    NavbarComponent,
    HomeComponent,
  ],
  imports: [
    MatExpansionModule,
    MatMenuModule,
    MatButtonModule,
    MatProgressBarModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MatIconModule,
    MatToolbarModule,
    MatTreeModule,
    MatMenuModule,
    HttpClientModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatDialogModule,
    MatFormFieldModule,
    MaterialModule,
    ToastrModule.forRoot(),
  ],

  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: { appearance: 'fill' },
    },
  ],

  bootstrap: [AppComponent],
})
export class AppModule {}
