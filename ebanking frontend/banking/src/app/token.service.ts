import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import jwtDecode from 'jwt-decode';
import { Observable, of } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class TokenService {
  private jwtHelper = new JwtHelperService();
  private token: string | null = null;
  private roles: any = null;
  constructor() {}
  isAuthenticated(): boolean {
    const token = localStorage.getItem('token') ?? '';
    return !this.jwtHelper.isTokenExpired(token);
  }
  getTokenPayload() {
    const token = localStorage.getItem('token');
    const tokenPayload = token !== null ? jwtDecode(token) : {};
    return tokenPayload;
  }

  public loadAccessToken(): string | null {
    return localStorage.getItem('token');
  }


  getRoles(): Observable<any> {
    const token = this.loadAccessToken();
  
    if (token) {
      const decodedToken = this.jwtHelper.decodeToken(token);
      const userRole = decodedToken.roles[0].authority;
      return of(userRole);
    }
  
    return of(null);
  }
}
