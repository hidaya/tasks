import { Component, OnInit, Inject } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CustomersService } from '../services/customers.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { map } from 'rxjs';
@Component({
  selector: 'app-edit-customer-details',
  templateUrl: './edit-customer-details.component.html',
  styleUrls: ['./edit-customer-details.component.scss'],
})
export class EditCustomerDetailsComponent implements OnInit {
  customerForm!: FormGroup;
  buttonClicked: boolean = false;
  user: any;
  userId!: number;
  constructor(
    private toastr: ToastrService,
    private formBuilder: FormBuilder,
    private customerService: CustomersService,
    private dialogRef: MatDialogRef<EditCustomerDetailsComponent>,
    private router: Router,
    //récupérer les données passées en entrée dans la boîte de dialogue.
    @Inject(MAT_DIALOG_DATA) public editData: any
  ) {}

  ngOnInit(): void {
    this.userId = this.editData.id;
    console.log(this.userId);
    this.customerForm = this.formBuilder.group({
      firstName: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
      ]),
      lastName: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
      ]),
      email: new FormControl('', [
        Validators.required,
        Validators.email,
        Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
      ]),
      adress: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
      ]),

      phone: new FormControl('', [
        Validators.required,
        Validators.pattern('^[0-9]*$'),
        Validators.minLength(8),
        Validators.maxLength(8),
      ]),
      job: new FormControl('', Validators.required),
    });
    this.customerService.getCustomerId(this.userId).subscribe((data) => {
      this.user = data;
      this.customerForm.patchValue({
        firstName: data.firstName,
        lastName: data.lastName,
        phone: data.phone,
        email: data.email,
        job: data.job,
        adress: data.adress,
      });
    });
    if (this.editData) {
      this.customerForm.controls['firstName'].setValue(this.editData.firstName);
      this.customerForm.controls['lastName'].setValue(this.editData.lastName);
      this.customerForm.controls['email'].setValue(this.editData.email);
      this.customerForm.controls['adress'].setValue(this.editData.adress);
      this.customerForm.controls['phone'].setValue(this.editData.phone);
      this.customerForm.controls['job'].setValue(this.editData.job);
    }
  }
  close() {
    this.customerForm.reset();
    this.dialogRef.close('save');
  }

  onSubmitForm() {
    this.customerForm.value.id = this.userId;
    this.customerService.editCustomer(this.customerForm.value).subscribe(
      (data) => {
        if (data != null) console.log(data);

        this.user = data;
        this.customerForm.reset();
        this.dialogRef.close('save');
        this.toastr.success('Client mis à jour avec succès!');
      },
      (error) => {
        if (error != null) {
          if (error.status === 400) {
            this.toastr.error('Répéter votre mise à jour !');
          } else {
            this.toastr.error('Une erreur est survenue, réessayez plus tard !');
          }
        }
      }
    );
  }
}
