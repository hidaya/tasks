import { Component, OnInit, Inject } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CustomersService } from '../services/customers.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-edit-customer',
  templateUrl: './edit-customer.component.html',
  styleUrls: ['./edit-customer.component.scss'],
})
export class EditCustomerComponent implements OnInit {
  customerForm!: FormGroup;
  buttonClicked: boolean = false;
  constructor(
    private toastr: ToastrService,
    private formBuilder: FormBuilder,
    private customerService: CustomersService,
    private dialogRef: MatDialogRef<EditCustomerComponent>,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) public editData: any
  ) {}

  ngOnInit(): void {
    this.customerForm = this.formBuilder.group({
      firstName: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
      ]),
      lastName: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
      ]),
      email: new FormControl('', [
        Validators.required,
        Validators.email,
        Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
      ]),
      adress: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
      ]),

      phone: new FormControl('', [
        Validators.required,
        Validators.pattern('^[0-9]*$'),
        Validators.minLength(8),
        Validators.maxLength(8),
      ]),
      job: new FormControl('', Validators.required),
    });
    if (this.editData) {
      this.customerForm.controls['firstName'].setValue(this.editData.firstName);
      this.customerForm.controls['lastName'].setValue(this.editData.lastName);
      this.customerForm.controls['email'].setValue(this.editData.email);
      this.customerForm.controls['adress'].setValue(this.editData.adress);
      this.customerForm.controls['phone'].setValue(this.editData.phone);
      this.customerForm.controls['job'].setValue(this.editData.job);
    }
  }
  close() {
    this.customerForm.reset();
    this.dialogRef.close('save');
  }
  onSubmitForm() {
    if (this.customerForm.valid) {
      this.customerService.addCustomer(this.customerForm.value).subscribe(
        (res) => {
          if ((res = !null)) this.toastr.success('Client ajouté avec succés!!');
          this.customerForm.reset();
          this.dialogRef.close('save');
        },
        (error) => {
          if (error != null) {
            if (error.status === 400) {
              this.toastr.error('Répéter votre mise à jour !');
            } else {
              this.toastr.error(
                'Une erreur est survenue, réessayez plus tard !'
              );
            }
          }
        }
      );
    }
  }
}
